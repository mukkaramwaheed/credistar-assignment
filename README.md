## Setup

Install Docker https://www.docker.com/get-started

Update your vendor packages

    docker-compose run --rm php composer update --prefer-dist
    
Run the installation triggers (creating cookie validation code)

    docker-compose run --rm php composer install    
    
Start the container

    docker-compose up -d
    
Run database migration (creating tables)

    docker-compose run --rm php tests/bin/yii migrate    
  
  
Import data into the tables using below command
  
    docker-compose run --rm php yii user-migration/index
    docker-compose run --rm php yii loan-migration/index
    
You can then access the application through the following URL:

    http://127.0.0.1:8000
    
Run this command to execute tests:

    docker-compose run --rm php codecept run