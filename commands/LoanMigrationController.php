<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;
use app\models\Loan;

use Yii;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class LoanMigrationController extends Controller
{
    public $dateFormat = 'M-d-Y';

    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */

    public function actionIndex()
    {

        //read the loans files
        if(file_exists("loans.json")){
            $string         = file_get_contents("loans.json");
            $data           = array();
            $loansData      = json_decode($string,true);

            //if data exist in file
            if($loansData){
                foreach($loansData as $value)
                {
                        $data[] = [
                            $value['user_id'] ?: '',
                            $value['amount'] ?: '',$value['interest'] ?: '',$value['duration'] ?: '',
                            date($this->dateFormat , $value['start_date']) ?: '',date($this->dateFormat , $value['end_date']) ?: '',$value['campaign'] ?: 0,
                            $value['status'] ?: ''
                        ];
                }

                Yii::$app->db->createCommand()
                    ->batchInsert('loan',
                        ['user_id',
                            'amount','interest','duration',
                            'start_date','end_date','campaign',
                            'status'],$data
                    )
                    ->execute();

                return ExitCode::OK;
            }
        }
        else{

            return ExitCode::OSFILE;
        }

    }
}
