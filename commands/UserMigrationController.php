<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;
use app\models\User;

use Yii;
use yii\db\Exception;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class UserMigrationController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionIndex()
    {

        //read the users files
        if(file_exists("users.json")){
            $string         = file_get_contents("users.json");
            $data           = array();
            $usersData      = json_decode($string,true);

            //if data exist in file
            if($usersData){

                foreach($usersData as $value)
                {
                    $findData = array(
                        'personal_code'         => $value['personal_code'],
                        'email'                 => $value['email']
                    );

                    //check email and personal code already exist or not to remove duplication
                    $recordExist = User::find()->where($findData)->one();

                    if(!$recordExist)
                    {
                        $data[] = [
                            $value['id'] ?: '',
                            $value['first_name'] ?: '',$value['last_name'] ?: '',$value['email'] ?: '',
                            $value['personal_code'] ?: '',$value['phone'] ?: '',$value['active'] ?: '',
                            $value['dead'] ?: '',$value['lang'] ?: ''
                        ];
                    }

                }

                Yii::$app->db->createCommand()
                             ->batchInsert('user',
                             ['id',
                             'first_name','last_name','email',
                             'personal_code','phone','active',
                             'dead','lang'],$data
                )
                    ->execute();

                return ExitCode::OK;
            }


        }
        else{

            return ExitCode::OSFILE;
        }

    }
}
