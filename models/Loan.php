<?php

namespace app\models;
use app\common\MyHelpers;
use app\models\User;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "loan".
 *
 * @property int $id
 * @property int $user_id
 * @property float $amount
 * @property float $interest
 * @property int|null $duration
 * @property int|null $start_date
 * @property int|null $end_date
 * @property int|null $campaign
 * @property int|null $status
 */
class Loan extends \yii\db\ActiveRecord
{
//    public $dateFormat = 'M-d-Y';


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'loan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'amount', 'interest','start_date', 'end_date',], 'required'],
            ['user_id', 'userAgaValidate'],
            [['amount','interest'], 'amountValidate'],
            [['duration','campaign'], 'match',
                'pattern' => '/^[0-9\s]+$/',
                'message' => 'Only contain numeric values'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User',
//            'user_id' => 'User ID',
            'amount' => 'Amount',
            'interest' => 'Interest',
            'duration' => 'Duration',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'campaign' => 'Campaign',
            'status' => 'Status',
        ];
    }



    public function getUser(){
        return $this->hasOne(User::className(),['id' => 'user_id']);
    }


    /**
     * Check amount is in correct format
     * @param integer $attribute
     * @return errorMessage
     */
    public function amountValidate($attribute)
    {
        if (!preg_match('/^\d{0,8}\.?((00))?$/', $this->$attribute)) {
            $this->addError($attribute, 'Number something like this 12345678.00 OR 123 etc ');
        }
    }

    /**
     * Check user is under age or not on form submit
     * @param integer $attribute
     * @return errorMessage
     */
    public function userAgaValidate($attribute)
    {
        $user = User::findOne($this->$attribute);

        if($user){
            $age = $this->loanApproval($user->personal_code);
            if(!$age){
                $this->addError($attribute, 'User is under age and not apply for loan');
            }
        }
        else{
            $this->addError($attribute, 'User not found');
        }


    }

    /**
     * Check personalCode valid or not. If valid return age
     * @param integer $personalCode
     * @return bool
     */

    public function loanApproval($personalCode){

        if($personalCode){
          $age =  MyHelpers::getAge($personalCode);

          if((is_numeric($age)) && ($age >= Yii::$app->params['underAge']))
          {
              return true;
          }
          else{
              return false;
          }
        }
        else{

            return false;
        }
    }






}
