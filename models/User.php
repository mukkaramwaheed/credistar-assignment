<?php

namespace app\models;
// use app\models\Loan;
use app\common\MyHelpers;
use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property int|null $personal_code
 * @property int|null $phone
 * @property int $active
 * @property int $dead
 * @property string $lang
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'email','personal_code','phone'], 'required'],
            [['personal_code', 'phone', 'active', 'dead'], 'integer'],

            [[ 'email', 'lang'], 'string', 'max' => 255],
            [['email'], 'unique'],
            ['active', 'default', 'value' => 1],
            ['dead', 'default', 'value' => 0],
            ['lang', 'default', 'value' => 'est'],

            ['personal_code', 'string', 'min'=>11, 'max'=>11, 'message' => '{attribute} should be at least 11 digits'],

            ['phone', 'string', 'min' => 8, 'max' => 8 , 'message' => '{attribute} should be at least 8 digits'],

            [['first_name', 'last_name'], 'string', 'min' => 3, 'max' => 255 ],
            [['first_name','last_name'], 'match',
            'pattern' => '/^[a-zA-Z\s]+$/',
            'message' => 'Only contain word characters'],



        ];
    }


//    public function afterFind()
//    {
//        $this->personal_code = MyHelpers::getAge($this->personal_code);
//        return parent::afterFind();
//    }
//

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'email' => 'Email',
            'personal_code' => 'Personal Code',
            'phone' => 'Phone',
            'active' => 'Active',
            'dead' => 'Dead',
            'lang' => 'Lang',
        ];
    }

    public function getLoan(){
        return $this->hasMany(Loan::className(),['user_id' => 'id']);
    }


}