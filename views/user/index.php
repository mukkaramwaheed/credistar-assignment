<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Users', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'first_name',
            'last_name',
            'email:email',
//            'personal_code',
            [
                'attribute' => 'personal_code',
                'label' => 'Age',
                'value' => function ($model, $key, $index, $column) { //Make use of the construct
                    $personal_code = $model->personal_code;
                   return \app\common\MyHelpers::getAge($personal_code);
                }

            ],
            'phone',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
