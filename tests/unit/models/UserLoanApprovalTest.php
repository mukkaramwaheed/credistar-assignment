<?php

namespace tests\models;

use app\models\Loan;
use app\common\MyHelpers;
use Codeception\Specify;

class UserLoanApprovalTest extends \Codeception\Test\Unit
{

    private $model;

    public function testInvalidPersonalCode()
    {
        $personalCode = '1111';
        $this->model = $this->getMockBuilder('app\models\Loan')
                        ->setMethods(['validate'])
                        ->getMock();

        expect_not($this->model->loanApproval($personalCode));

    }

    public function testApplyForLoan()
    {
        $personalCode = '49005025465';
        $this->model = $this->getMockBuilder('app\models\Loan')
            ->setMethods(['validate'])
            ->getMock();

        expect_that($this->model->loanApproval($personalCode));
    }

    public function testInvalidAge()
    {

        $personalCode = '79005025465';
        $this->model = $this->getMockBuilder('app\models\Loan')
            ->setMethods(['validate'])
            ->getMock();

        expect_not($this->model->loanApproval($personalCode));
    }



}
