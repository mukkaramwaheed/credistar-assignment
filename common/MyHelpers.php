<?php
namespace app\common;
use DateTime;

class MyHelpers
{

    /**
     * Get personalCode and extract the year from it
     * @param integer $personalCode
     * @return age
     */
    public static function getAge($personal_code) {


        if(($personal_code) && (strlen($personal_code) == 11)) {

            $digit  = substr($personal_code, 0, 1);
            $year   = substr($personal_code, 1, 2);
            $month  = substr($personal_code, 3, 2);
            $day    = substr($personal_code, 3, 2);
            $century = '';

            if ($digit == 1 || $digit == 2) {
                $century = 18;
            } elseif ($digit == 3 || $digit == 4) {
                $century = 19;
            } elseif ($digit == 5 || $digit == 6) {
                $century = 20;
            }
            if($century){

                $dateOfBirth = $century . $year . '-' . $month . '-' . $day;

                $dateOfBirth = new DateTime($dateOfBirth);
                $currentDate = new DateTime();

                $diff = $dateOfBirth->diff($currentDate);

                return $diff->y;
            }
            else{
                return false;
            }

        }
        else {
            return false;
        }

    }
}