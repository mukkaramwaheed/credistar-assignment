<?php

return [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'tester@example.com',
    'senderName' => 'tester',
    'dateFormat' => 'm-d-Y',
    'underAge' => '18',
];
