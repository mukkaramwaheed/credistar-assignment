<?php


return [
    'class' => \yii\db\Connection::class,
    'dsn' => 'pgsql:host=db_test;dbname=example',
    // 'dsn' => 'pgsql:host=0.0.0.0;port=5432;dbname=example',
    // 'dsn' => 'pgsql:host=localhost;port=5432;dbname=example',
    'username' => 'example',
    'password' => 'example',
    'charset' => 'utf8'
];



// return [
  
//   'components' => [
     
//         'class' => \yii\db\Connection::class,
//           'dsn' => 'pgsql:host=localhost;dbname=example',
//           'username' => 'example',
//           'password' => 'example',
//           'schemaMap' => [
//               'pgsql'=> 'tigrov\pgsql\Schema',
//           ],
     
//   ],
// ];